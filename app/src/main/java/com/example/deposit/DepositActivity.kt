package com.example.deposit

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.example.eventbus.EventBusModel
import org.example.eventbus.MessageEvent


class DepositActivity : AppCompatActivity() {

    lateinit var textView: TextView
    lateinit var amountEditText: TextInputEditText
    lateinit var amountInputLayout: TextInputLayout
    var currentIdSelected = 0
    var amount = -100000.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val arguments = intent.extras
        val name = arguments!!["amount"].toString()

        setContentView(R.layout.activity_deposit)

        amount = name.toDouble()

        textView = findViewById(R.id.amountDepositTextView)
        handleEvent(MessageEvent.MessageAmount(amount))

        val items = listOf("bill 1", "bill 2", "bill 3", "bill 4")
        val adapter = ArrayAdapter(applicationContext, R.layout.support_simple_spinner_dropdown_item, items)
        val dropdownMenu = findViewById<AutoCompleteTextView>(R.id.whereAutoCompleteText)
        dropdownMenu.setAdapter(adapter)
        dropdownMenu.setOnItemClickListener { parent, view, position, id ->
            currentIdSelected = id.toInt()
        }

        amountEditText = findViewById(R.id.depositInputEditText)
        amountInputLayout = findViewById(R.id.depositTextInputLayout)

        lifecycleScope.launch {
            lifecycle.repeatOnLifecycle(Lifecycle.State.CREATED) {
                EventBusModel.events.collect {
                    withContext(Dispatchers.Main) {
                        Log.e("before handle", "")
                        handleEvent(it)
                    }
                }
            }
        }

        findViewById<Button>(R.id.okDepositButton).setOnClickListener {
            val amountSub = amountEditText.text.toString().trim()

            if (amountSub.isEmpty() || amountSub.toDouble() <= 0.0) {
                amountInputLayout.error = "Amount must be greater than zero!"
                amountInputLayout.requestFocus()
                return@setOnClickListener
            }
            handleEvent(MessageEvent.MessageAmount(amount - amountSub.toDouble()))

            val intent = Intent()
            intent.setClassName(this, "com.example.appbank.MainActivity")
            intent.putExtra("amount", (amount - amountSub.toDouble()).toString())
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            /*MaterialAlertDialogBuilder(this).setTitle("Confirm transaction")
                    .setMessage("Add $amountSub on ${items[currentIdSelected]}")
                    .setNegativeButton(resources.getString(R.string.decline)) { dialog, which ->
                    }
                    .setPositiveButton(resources.getString(R.string.accept)) { dialog, which ->

                    }
                    .show()*/
        }

    }

    private fun handleEvent(event: MessageEvent) =
        when (event) {
            is MessageEvent.MessageAmount -> {
                amount = event.amount
                textView.text = "Баланс $amount"
            }
            else -> {}
        }

    override fun onDestroy() {
        lifecycleScope.launch {
            EventBusModel.produceEventSus(MessageEvent.MessageAmount(amount))
        }
        super.onDestroy()

        lifecycleScope.launch {
            EventBusModel.produceEventSus(MessageEvent.MessageAmount(amount))
        }
    }
}